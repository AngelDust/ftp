<?php

namespace App\Manager\Files;

use Symfony\Component\Finder\Finder;

class FilesDirectory {
    
    public function showDirectory($inDir, Finder $finder){
        return $this->finder = $finder->files()->in($this->inDir=$inDir); 
    }

}
