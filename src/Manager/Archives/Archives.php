<?php

namespace App\Manager\Archives;

use \ZipArchive;

class Archives {
    
    public function createZip(string $user, string $directory,array $files,\ZipArchive $zip){
        $this->zip = $zip;
        $this->directory = $directory;
        $this->user = $user;
        $this->zip->open($this->directory.'klient_'.$this->user.'.zip', ZipArchive::CREATE);
        $this->files = $files;
        for($x = 0; $x < count($this->files); $x++){
           $this->zip->addFile($this->directory.$this->user.'/'.$this->files[$x], $this->files[$x]);
        }
        $this->zip->close();

        return $this->directory.'klient_'.$this->user.'.zip';
    }
}
