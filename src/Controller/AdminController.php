<?php

namespace App\Controller;

use App\Form\UserType;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Manager\Files\FilesDirectory;
use Symfony\Component\Finder\Finder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Manager\Archives\Archives;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function admin(Request $request,UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer)
    {         
        $user = new User();
        $dirPath = $this->get('kernel')->getProjectDir() . '/public/filetransfer/';
        $generatePasswd = $this->getRandomPassword();
        $form = $this->createForm(UserType::class, $user);
        $userList = $this->customersList();
        
        $form->handleRequest($request);
        $user->setPassword( $generatePasswd);
        $user->setPlainPassword($generatePasswd);
        
        if ($form->isSubmitted() && $form->isValid()) { 
            $message = (new \Swift_Message('Itengineering - Konto'))
               ->setFrom("support@iten.com.pl")
               ->setTo(array($user->getEmail() => $user->getUsername()))
               ->setBody(
               $this->renderView(
                'email/register.html.twig',[
                    'customerLogin' => $user->getUsername(),
                    'customerPasswd' => $user->getPlainPassword(),
                ]
            ),
            'text/html'
               );
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            if(!file_exists($dirPath.$user->getUsername())){
                mkdir($dirPath.$user->getUsername(), 0755);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $mailer->send($message);
            
            $this->addFlash('success','Klient został dodany!');

            return $this->redirectToRoute('admin');
  
        }
             
        return $this->render('admin/admin.html.twig', [
            'user' => $this->getUser(),
            'form' => $form->createView(),
            'customers' => $userList
        ]);
    }
    
     /**
     * @Route("/admin/{slug}", name="admin_manager")
     */
    public function adminManager(string $slug, FilesDirectory $filesDirectory){
        $directory = $this->getParameter('kernel.project_dir') . '/public/filetransfer/'.$this->slug = $slug.'/';
        $entityManager = $this->getDoctrine()->getRepository(User::class);
        $userDir = $entityManager->findOneBy([
           'username' => $this->slug = $slug 
        ]);
        
        return $this->render('admin/user_manager.html.twig', [
            'userFilesInDirectory'=> $filesDirectory->showDirectory($directory, new Finder()),
            'userDir' => $userDir,
            'user' => $this->getUser(),
        ]);
        
    }

    protected function getRandomPassword($length = 12){
                $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                $pass = array(); 
                $alphaLength = strlen($alphabet) - 1; 
                for ($i = 0; $i < $length; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];
                }
                return implode($pass); 
    }
    
    public function customersList(){
        $entityManager = $this->getDoctrine()->getRepository(User::class);
        $list = $entityManager->findAll();
        return $list;
    }
     /**
     * @Route("/download", name="download")
      *@Method({"POST"})
     */
    public function downloadFiles(Request $request, Archives $archives){
                $user = $request->request->get('userDir');
                $form = $request->request->get('archives');
                $dirPath = $this->get('kernel')->getProjectDir().'/public/filetransfer/';
                $zipFile = $archives->createZip($user,$dirPath, $form, new \ZipArchive());
                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename='."klient_".$user.'.zip');
                header('Content-Length: ' . filesize($zipFile));
                readfile($zipFile);
                  return $this->redirectToRoute('admin');
  
    }
    
    
    

}
