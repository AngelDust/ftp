<?php

namespace App\Controller; 

use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\Files\FilesDirectory;
use Symfony\Component\Finder\Finder;


class AccountController extends Controller{
    /**
     * @Route("/account")
     * 
     */
    public function account(Request $request,FilesDirectory $filesDirectory){  
        
        $data = null;
        $user = $this->getUser();
        $directory = $this->getParameter('kernel.project_dir') . '/public/filetransfer/'.$user->getUsername().'/';
        $files = $this->createForm(UploadType::class, $data);
        $files->handleRequest($request);
        $files->get('attachment');
        if($files->isSubmitted() && $files->isValid()) {
              foreach ($files['attachment']->getData() as $fileArrayElement) {
                  $fileArrayElement->move($directory, str_replace(" ", "_", $fileArrayElement->getClientOriginalName())); 
              } 
          $this->addFlash(
            'success','Pliki zostały pomyslnie zapisane!'
        );
        }
        
        if($this->isGranted("ROLE_ADMIN")){
                  return $this->redirectToRoute('admin');
        }else{
          return $this->render('account/account.html.twig',
                    array(
                        'userFilesInDirectory'=> $filesDirectory->showDirectory($directory, new Finder()),
                        'user' => $user,
                        'files' => $files->createView() 
                    ));
        }

    }
      


}