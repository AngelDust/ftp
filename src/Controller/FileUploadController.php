<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\File\File;


class FileUploadController {
   
    
    private $userDir;

    
    public function getUserDir(){
        return $this->userDir;
    }
    
    public function setUserDir($userDir){
        $this->userDir = $userDir;      
    }
    
    
}
