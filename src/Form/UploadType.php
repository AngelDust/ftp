<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;




class UploadType extends AbstractType
{
public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder
        ->add('attachment', FileType::class, array(
            'label' => 'Wybierz pliki do przesłania',
            'multiple' => true, 
            'required' => false,
            'data_class' => null,
            'error_bubbling' => true,
            'attr' => array('class' => 'custom-file-input'),
            'constraints' =>  array(
                new Assert\All(
                        array(
                            new Assert\NotBlank(array('message' => "Wybierz pliki do przesłania")),
                            new Assert\File(array('maxSize' => '20M', 'mimeTypes' => 
                                array("application/pdf", 
                                      "application/x-pdf", 
                                      "image/jpeg", 
                                      "image/png",
                                      "image/gif",
                                      "image/jpg",
                                      "application/zip", 
                                      "application/rar",
                                      "application/msword",
                                      "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
                                'mimeTypesMessage' => "Nie prawidłowy format pliku"))
                ))
        )))
                
        ->add('send', SubmitType::class, array(
            'label'=> 'ZAPISZ WYBRANE PLIKI'
        ))
        ->getForm();
}

}